.PHONY: infragit,updategit

UPSTREAMGIT=https://infrastructure.fedoraproject.org/infra/ansible.git

init: infra-ansible/
	mkdir private

updategit: infra-ansible/
	git -C infra-ansible pull

infra-ansible/:
	git clone $(UPSTREAMGIT) infra-ansible

allinone:
	ansible-playbook playbooks/taskotron-allinone.yml

sslcerts:
	echo "TBD: make some ssl certs"

sshcerts:
	echo "TBD: generate ssh certs"

secret_keys:
	echo "TBD: generate app secret keys"
