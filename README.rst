Taskotron Standalone Playbook
=============================

This is a set of ansible playbooks and support files to deploy a Taskotron instance
outside of Fedora infrastructure.

At this point, this stuff is **very** young and somewhat user-unfriendly. Feel
free to use the playbooks but there are still undocumented steps, so be prepared
to figure some bits out.

If you run into problems or have questions, folks who can help can be found on
Freenode IRC (#fedora-qa) or on the `qa-devel mailing list <https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_.


Do You Need a Complete Deployment?
----------------------------------

If you are looking to write tasks for Taskotron, you should not need an entire
deployment of Taskotron - `libtaskotron <https://bitbucket.org/fedoraqa/libtaskotron>`_
is designed to support the local execution use case. If what you're trying to
write can't be executed - please let us know so we can fix the problem.

If we've done our job correctly, the only folks who should need an entire
deployment are core devs working on the interaction between Taskotron components
and folks looking to demo the system as a whole.


Basic Usage
-----------

The playbooks are designed to deploy all taskotron components on a single
machine. For best results, do a fresh install of Fedora 20 (web server package
set) and after install, update the host and set up ssh key auth from your user
to root on the VM. Note the IP address and ssh host key of the vm - you'll need
it when setting up the playbook.

To set up the playbooks after cloning, you will need to do a few manual setup steps.

* run ``make init`` to clone the production playbooks

  - will also create the ``private/`` directory if it doesn't already exist

* change ``basedir`` in ``common/vars/global.yml`` to be the absolute path to
  your local checkout
* hostname config
* fill out private vars template (``docs/private-vars-template.yml``) and put
  it in ``private/vars.yml``
* change the values in ``inventory/inventory`` to match your VM's IP address

  - Alternately, you could set an alias in ``/etc/hosts`` so that ``taskotron-local``
    resolves to your VM. This way you don't have to alter the contents of the
    inventory file.


Tips
----

Generate secret keys for Flask apps with::

  python -c "import os; print os.urandom(24).encode('string_escape')"

Generate plaintext passwords with::

  python -c "import string; import random; print''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(32)])"

Generate crypted passwords for system users (``python-passlib`` is needed)::

  python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.encrypt(getpass.getpass())"

Step-by-step instructions
-------------------------

For a step-by-step guide on deploying the taskotron in you local VM using these playbooks have a look at the StepByStep.rst file in this repository.

Still TODO
----------

There is still quite a bit of stuff that needs to be done to make these playbooks
more user-friendly:

* generate buildslave ssh certs
* generate ssl certs
* generate passwords
* generate app secret keys
* auto-fill hostname stuff
