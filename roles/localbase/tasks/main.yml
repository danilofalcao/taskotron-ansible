- name: ensure base packages are installed
  action: yum name={{ item }} state=latest
  with_items:
      - openssl
      - screen
      - vim
      - openssh-clients
      - system-config-firewall-base
      - libsemanage-python
      - libselinux-python
      - policycoreutils-python
      - chrony

- name: ensure firewalld package is installed(fedora)
  action: yum name=firewalld state=latest
  when: is_fedora

- name: copy screen config
  copy: src=screenrc dest=/root/.screenrc

- name: configure sudo
  copy: src=sudoers dest=/etc/sudoers owner=root group=root mode=0440

- name: set root password
  user: name=root password={{rootpw}}

- name: setup taskotron copr repo
  copy: src=copr-taskotron.repo dest=/etc/yum.repos.d/copr-taskotron.repo owner=root group=root mode=0644

- name: stop and disable iptables (fedora)
  service: name={{ item }} enabled=no state=stopped
  with_items:
      - iptables
      - ip6tables
  when: is_fedora

- name: ensure firewalld service is unmasked
  file: path=/etc/systemd/system/firewalld.service state=absent
  when: is_fedora

- name: start and enable firewalld (fedora)
  service: name=firewalld enabled=yes state=started
  when: is_fedora

- name: open up ports in firewall (fedora)
  action: command firewall-cmd --permanent {{ item }}
  with_items:
    - --add-service=ssh
    - --add-port={{sshd_port}}/tcp
  when: is_fedora
  notify:
    - restart firewalld

- name: open up ports in firewall (rhel)
  action: command lokkit -p '{{item}}:tcp'
  with_items:
  - 22
  - '{{sshd_port}}'
  when: is_rhel

- name: set hostname (rhel)
  when: is_rhel
  template: src=rhel-sysconfig-network.conf.j2 dest=/etc/sysconfig/network owner=root group=root mode=0644

- name: check to see if sshd port is already known by selinux
  shell: semanage port -l | grep ssh
  register: sshd_selinux_port

- name: allow alternate sshd port
  command: semanage port -a -t ssh_port_t -p tcp {{ sshd_port }}
  when: sshd_selinux_port.stdout.find('{{ sshd_port }}') == -1

- name: update sshd config
  template: src=sshd_config.j2 dest=/etc/ssh/sshd_config owner=root group=root mode=0600
  notify:
    - restart sshd

- name: install common scripts from infra-ansible
  copy: src={{ item }} dest=/usr/local/bin owner=root group=root mode=0755
  with_fileglob:
   - "{{ infra_ansible }}/roles/base/files/common-scripts/*"
