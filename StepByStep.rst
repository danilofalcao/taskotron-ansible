Preparation
===========

* Get a VM with a F20 server (webserver package selection) installed & updated

To-Do on the host machine
=========================

* OPTIONAL: Set up an alias pointing 'taskotron-local' to the VM
* Run following::

    ssh-copy-id -i #path to key #root@taskotron-local
    git clone https://bitbucket.org/fedoraqa/taskotron-ansible
    cd taskotron-ansible
    make init

* Change basedir in ``common/vars/global.yml`` to be the absolute path to your local checkout
* Copy ``docs/private-vars-template.yml`` to ``private/vars.yml`` and fill it in:

  * use ``yum install python-pip ; pip install passlib``
  * use ``python -c "from passlib.hash import sha512_crypt; import getpass; print sha512_crypt.encrypt(getpass.getpass())"`` for the ``rootpw``
  * set the ``deployment_host`` to ``taskotron-local`` (or to the actual IP if you have not set the alias)
  * use ``python -c "import os; print os.urandom(24).encode('string_escape')"`` for ``*_secret_key``
  * use ``python -c "import string; import random; print''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(32)])"`` for ``*_password``
  * copy the content of the VM's ``/etc/ssh/ssh_host_ecdsa_key`` or ``/etc/ssh/ssh_host_rsa_key.pub`` to the ``buildmaster_pubkey``
  * run::

     mkdir -p private/files/taskotron/local-buildslave-sshkey
     cd private/files/taskotron/local-buildslave-sshkey
     ssh-keygen -f local_buildslave # set an empty passphrase
     cd ../../../../
  * copy the content of ``private/files/taskotron/local-buildslave-sshkey/local_buildslave.pub`` to the ``buildslave_ssh_pubkey``
  * If you have not set the 'taskotron-local' alias, update the ``inventory/inventory`` file with the VM's IP address
* run::

    mkdir -p private/certs/local
    scp root@taskotron-local:/etc/pki/tls/private/localhost.key private/certs/local/ssl.key
    scp root@taskotron-local:/etc/pki/tls/certs/localhost.crt private/certs/local/ssl.crt
    ansible-playbook -u root playbooks/taskotron-allinone.yml

* If you get ``TASK: [taskotron/buildmaster-configure | start and enable buildmaster service]`` FAIL, then log into the VM and re-run the ``ansible-playbook`` command after::

    sudo su - buildmaster
    buildbot upgrade-master master
    logout

To-Do on the VM
===============

Initialize the resultsdb's and fakefedorainfra's databases::

  PROD=true resultsdb init_db
  yum install pytest
  fake_fedorainfra init_db

Check that the git repos are downloaded in ``/var/lib/git/mirror``, if not, then::

  cd /var/lib/git/mirror
  mkdir fedoraqa
  cd fedoraqa
  git clone --bare https://bitbucket.org/fedoraqa/task-rpmlint.git rpmlint
  git clone --bare https://bitbucket.org/fedoraqa/task-depcheck.git depcheck
  git clone --bare https://bitbucket.org/fedoraqa/task-upgradepath.git upgradepath
  cd
  chown -R grokmirror:grokmirror /var/lib/git/mirror/

Notes
-----

Should you want to disable the trigger::

  systemctl stop fedmsg-hub
  systemctl disable fedmsg-hub
